#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import feedparser
import json
import sys
import urllib3


class Source:

    def __init__(self):
        self.url = None
        self.title = None
        self.image_url = None
        self.themes = None
        self.languages = None

    def parse_json(self, json_input):
        if 'url' in json_input:
            self.url = json_input['url']

        if 'themes' in json_input:
            self.themes = json_input['themes']

        if 'languages' in json_input:
            self.languages = json_input['languages']

    def is_valid(self):
        return self.url is not None

    def parse_xml(self, xml_data):
        print("Parsing feed at url %s..." % self.url)
        data = feedparser.parse(xml_data)

        if 'feed' in data and data['feed'] is not None:
            feed = data['feed']

            if 'title' in feed:
                t = feed['title']

                if t is not None and len(t) > 0:
                    print('Found title: %s' % t)
                    self.title = t

            if 'image' in feed:
                img = feed['image']

                if img is not None and 'href' in img:
                    url = img['href']

                    if url is not None and len(url) > 0:
                        print('Found image url: %s' % url)
                        self.image_url = url

    def to_json_object(self):
        result = {}

        if self.url is not None and len(self.url) > 0:
            result['url'] = self.url

        if self.title is not None and len(self.title) > 0:
            result['title'] = self.title

        if self.image_url is not None and len(self.image_url) > 0:
            result['image_url'] = self.image_url

        if self.themes is not None and len(self.themes) > 0:
            result['themes'] = self.themes

        if self.languages is not None and len(self.languages) > 0:
            result['languages'] = self.languages

        return result


if __name__ == '__main__':
    print('Updating sources...')

    sources = []

    with open('raw-sources.json') as json_sources:
        raw_sources = json.load(json_sources)

        for raw_source in raw_sources:
            source = Source()
            source.parse_json(raw_source)

            if source.is_valid():
                print('Adding %s to sources to proceed.' % source.url)
                sources.append(source)

    if len(sources) > 0:
        pm = urllib3.PoolManager()
        updated_sources = []

        for source in sources:
            xml = pm.request('GET', source.url)
            
            if xml.status == 200:
                source.parse_xml(xml.data)
            else:
                print('\033[91m/!\ Invalid source: %s.\033[0m' % source.url, file=sys.stderr)

            updated_sources.append(source.to_json_object())

        with open('sources.json', 'w') as outfile:
            json.dump(updated_sources, outfile, indent=2)

        with open('sources.min.json', 'w') as outfile:
            json.dump(updated_sources, outfile, separators=(',', ':'), indent=None)
